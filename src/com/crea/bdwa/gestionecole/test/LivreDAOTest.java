package com.crea.bdwa.gestionecole.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.crea.bdwa.gestionecole.beans.Livre;
import com.crea.bdwa.gestionecole.dao.LivreDAO;

class LivreDAOTest {

	@BeforeAll 
    public static void testAddListeLivre() throws ParseException {
        int insert = 1;        
        assertEquals(insert,LivreDAO.addLivre("A1", "AGUE001", "livre1", d));
        assertEquals(insert,LivreDAO.addLivre("A2", "LAURENCY004", "livre2", d));
        assertEquals(insert,LivreDAO.addLivre("A3", "TAHAE002", "livre3", d));
                
    }
    
    @AfterAll 
    public static void testDeleteListeLivre() {
        int delete = 1;
        
        assertEquals(delete, LivreDAO.deleteLivreByCote("A1"));
        assertEquals(delete, LivreDAO.deleteLivreByCote("A2"));
        assertEquals(delete, LivreDAO.deleteLivreByCote("A3"));
		System.out.println("Livre Supprimé ou pas : " + delete);
    }
	
	@Test
	void testGetCLivreByCote() throws SQLException, ParseException {
		java.util.Date d = sdf.parse("21/12/2012");
		Livre e_ref= new Livre("A1", "AGUE001", "livre1", d);
		Livre e_res = new Livre();
		e_res = LivreDAO.getLivreByCote("A1");	
		assertEquals(e_ref.getCote(),e_res.getCote());
		assertEquals(e_ref.getTitre(),e_res.getTitre());
	}


	@Test
	void testGetAllLivre() throws SQLException, ParseException {
		java.util.Date d = sdf.parse("21/12/2012");
		ArrayList<Livre> listLivre = new ArrayList<Livre>();

		Livre l1 = new Livre("A1", "AGUE001", "livre1", d);
		Livre l2 = new Livre("A2", "LAURENCY004", "livre2", d);
		Livre l3 = new Livre("A3", "TAHAE002", "livre3", d);

		listLivre.add(l1);
		listLivre.add(l2);
		listLivre.add(l3);
		//listEleve.add(e2);
		
		System.out.println("Taille Liste Date: "+LivreDAO.getAllLivre().size());
		for( int i =0; i < listLivre.size(); i++ )
		{
			assertEquals(listLivre.get(i).getCote(),LivreDAO.getAllLivre().get(i).getCote());
			assertEquals(listLivre.get(i).getNum(),LivreDAO.getAllLivre().get(i).getNum());
			assertEquals(listLivre.get(i).getTitre(),LivreDAO.getAllLivre().get(i).getTitre());

		}
	}

	@Test
	void testDeleteLivreByCote() throws ParseException {
		java.util.Date d = sdf.parse("21/12/2012");
		int delete = 1;
		int insert = 1;
        assertEquals(insert,LivreDAO.addLivre("C3", "KAMTO005", "livre6", d));
        assertEquals(delete,LivreDAO.deleteLivreByCote("C3"));
        assertNotEquals(delete,LivreDAO.deleteLivreByCote("Z2"));
	}


	@Test
	void testUpdTitreLivreByCote() throws SQLException, ParseException {
		java.util.Date d = sdf.parse("21/12/2012");
		int update = 1;
		Livre l = new Livre("A1", "AGUE001", "livreTEST", d);
        assertEquals(update,LivreDAO.updTitreLivreByCote("A1", "livreTEST"));
        assertEquals(l.getTitre(),LivreDAO.getLivreByCote("A1").getTitre());
        assertNotEquals(update,LivreDAO.updTitreLivreByCote("C4", "TABIS003"));
        assertEquals(update,LivreDAO.updTitreLivreByCote("A1", "livre1"));
	}


	@Test
	void testAddLivre() throws ParseException {
		java.util.Date d = sdf.parse("21/12/2012");
		int insert = 1;
        int delete = 1;
        assertEquals(insert,LivreDAO.addLivre("R2", "AGUE001", "livre21", d));
        assertNotEquals(insert,LivreDAO.addLivre("A1", "AGUE001", "livre1", d));
        assertEquals(delete,LivreDAO.deleteLivreByCote("R2"));
	}

}
