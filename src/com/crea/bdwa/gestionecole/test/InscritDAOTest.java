package com.crea.bdwa.gestionecole.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.crea.bdwa.gestionecole.beans.Inscrit;
import com.crea.bdwa.gestionecole.dao.EleveDAO;
import com.crea.bdwa.gestionecole.dao.InscritDAO;
import com.crea.bdwa.gestionecole.dao.UVDAO;

class InscritDAOTest {
	
	@BeforeAll 
    public static void testAddListeEleve() {
        int insert = 1;
        //création uv
        assertEquals(insert,UVDAO.addUV("JAVA_Grp1", 30, "Mr RIDENE"));
        assertEquals(insert,UVDAO.addUV("maths_info_Grp1", 26, "Mme ASSELAH"));
        assertEquals(insert,UVDAO.addUV("Web_Service_Grp1", 10, "Mr PLASSE"));
        //création élèves
        assertEquals(insert,EleveDAO.addEleve("AGUE001", "AGUE MAX", 40, "18 Rue Labat 75018 Paris"));
        assertEquals(insert,EleveDAO.addEleve("KAMTO005", "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy"));
        assertEquals(insert,EleveDAO.addEleve("LAURENCY004", "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris"));
        assertEquals(insert,EleveDAO.addEleve("TABIS003", "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris"));
        assertEquals(insert,EleveDAO.addEleve("TAHAE002", "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles"));
        //création inscrits
        assertEquals(insert,InscritDAO.addInscrit("JAVA_Grp1", "AGUE001", 4));
        assertEquals(insert,InscritDAO.addInscrit("JAVA_Grp1", "LAURENCY004", 6));
        assertEquals(insert,InscritDAO.addInscrit("maths_info_Grp1", "LAURENCY004", 4));
        assertEquals(insert,InscritDAO.addInscrit("Web_Service_Grp1", "TAHAE002", 4));
        assertEquals(insert,InscritDAO.addInscrit("Web_Service_Grp1", "TABIS003", 4));
        
        System.out.println("––––––START––––––");
    }
    
    @AfterAll 
    public static void testDeleteListeEleve() {
        int delete = 1;
        assertEquals(delete,InscritDAO.deleteInscritByCodeNum("JAVA_Grp1", "AGUE001"));
        assertEquals(delete,InscritDAO.deleteInscritByCodeNum("JAVA_Grp1", "LAURENCY004"));
        assertEquals(delete,InscritDAO.deleteInscritByCodeNum("maths_info_Grp1", "LAURENCY004"));
        assertEquals(delete,InscritDAO.deleteInscritByCodeNum("Web_Service_Grp1", "TAHAE002"));
        assertEquals(delete,InscritDAO.deleteInscritByCodeNum("Web_Service_Grp1", "TABIS003"));
        
        assertEquals(delete,EleveDAO.deleteEleveByNum("AGUE001"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("KAMTO005"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("LAURENCY004"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("TABIS003"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("TAHAE002"));
        
        assertEquals(delete,UVDAO.deleteUVByCode("JAVA_Grp1"));
        assertEquals(delete,UVDAO.deleteUVByCode("maths_info_Grp1"));
        assertEquals(delete,UVDAO.deleteUVByCode("Web_Service_Grp1"));
        System.out.println("––––––END––––––");
    }

	@Test
	void testGetInscritByCodeNum() throws SQLException {
		Inscrit e_ref= new Inscrit("JAVA_Grp1", "AGUE001", 4);
		Inscrit e_res = new Inscrit();
		e_res = InscritDAO.getInscritByCodeNum("JAVA_Grp1", "AGUE001");	
		System.out.println("Taille ListeUV by Code : "+InscritDAO.getAllInscrit().size());
		assertEquals(e_ref.getCode(),e_res.getCode());
		assertEquals(e_ref.getNum(),e_res.getNum());
		assertEquals(e_ref.getNote(),e_res.getNote());
	}

	@Test
	void testGetInscritByCode() throws SQLException {
		ArrayList<Inscrit> listInscrit = new ArrayList<Inscrit>();

		Inscrit i1 = new Inscrit("JAVA_Grp1", "AGUE001", 4);

		listInscrit.add(i1);
//		listEleve.add(e2);
		
		System.out.println("Taille Liste Nbh: "+InscritDAO.getInscritByCode("JAVA_Grp1").size());
		for( int i =0; i < listInscrit.size(); i++ )
		{
			assertEquals(listInscrit.get(i).getCode(),InscritDAO.getInscritByCode("JAVA_Grp1").get(i).getCode());
			assertEquals(listInscrit.get(i).getNum(),InscritDAO.getInscritByCode("JAVA_Grp1").get(i).getNum());
			assertEquals(listInscrit.get(i).getNote(),InscritDAO.getInscritByCode("JAVA_Grp1").get(i).getNote());
		}
	}

	@Test
	void testGetInscritByNum() throws SQLException {
		ArrayList<Inscrit> listInscrit = new ArrayList<Inscrit>();

		Inscrit i1 = new Inscrit("JAVA_Grp1", "AGUE001", 4);

		listInscrit.add(i1);
//		listEleve.add(e2);
		
		System.out.println("Taille Liste Nbh: "+InscritDAO.getInscritByNum("AGUE001").size());
		for( int i =0; i < listInscrit.size(); i++ )
		{
			assertEquals(listInscrit.get(i).getCode(),InscritDAO.getInscritByNum("AGUE001").get(i).getCode());
			assertEquals(listInscrit.get(i).getNum(),InscritDAO.getInscritByNum("AGUE001").get(i).getNum());
			assertEquals(listInscrit.get(i).getNote(),InscritDAO.getInscritByNum("AGUE001").get(i).getNote());
		}
	}

	@Test
	void testGetAllChambre() throws SQLException {
		ArrayList<Inscrit> listInscrit = new ArrayList<Inscrit>();

		Inscrit i1 = new Inscrit("JAVA_Grp1", "AGUE001", 4);

		listInscrit.add(i1);
//		listEleve.add(e2);
		
		System.out.println("Taille Liste Nbh: "+InscritDAO.getAllInscrit().size());
		for( int i =0; i < listInscrit.size(); i++ )
		{
			assertEquals(listInscrit.get(i).getCode(),InscritDAO.getAllInscrit().get(i).getCode());
			assertEquals(listInscrit.get(i).getNum(),InscritDAO.getAllInscrit().get(i).getNum());
			assertEquals(listInscrit.get(i).getNote(),InscritDAO.getAllInscrit().get(i).getNote());
		}
	}

	@Test
	void testDeleteInscritByCodeNum() {
		int delete = 1;
		int insert = 1;
        assertEquals(insert,InscritDAO.addInscrit("JAVA_Grp1", "TAHAE002", 3));
        assertEquals(delete,InscritDAO.deleteInscritByCodeNum("JAVA_Grp1", "TAHAE002"));
        assertNotEquals(delete,InscritDAO.deleteInscritByCodeNum("JAVA_Grp1", "TABIS003"));
	}

	@Test
	void testUpdNumInscritByCodeNum() throws SQLException {
		int update = 1;
		Inscrit i = new Inscrit("JAVA_Grp1", "Mr RIDENE", 6);
		
        assertEquals(update,InscritDAO.updNumInscritByCodeNum("JAVA_Grp1", "AGUE001", 6));
        assertEquals(i.getNote(),InscritDAO.getInscritByCodeNum("JAVA_Grp1", "AGUE001").getNote());
        assertNotEquals(update,InscritDAO.updNumInscritByCodeNum("Web_Service_Grp1", "AGUE001", 6));
        assertEquals(update,InscritDAO.updNumInscritByCodeNum("JAVA_Grp1", "AGUE001", 4));
	}

	@Test
	void testAddInscrit() {
		int insert = 1;
        int delete = 1;
        assertEquals(insert,InscritDAO.addInscrit("Web_Service_Grp1", "AGUE001", 3));
        assertNotEquals(insert,InscritDAO.addInscrit("JAVA_Grp1", "AGUE001", 3));
        assertEquals(delete,InscritDAO.deleteInscritByCodeNum("Web_Service_Grp1", "AGUE001"));
	}

}
