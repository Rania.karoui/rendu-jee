package com.crea.bdwa.gestionecole.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterAll;

import com.crea.bdwa.gestionecole.beans.Eleve;
import com.crea.bdwa.gestionecole.dao.EleveDAO;

class EleveDAOTest {
	
	@BeforeAll
    public static void testAddListeEleve() {
        int insert = 1;
        assertEquals(insert,EleveDAO.addEleve("AGUE001", "AGUE MAX", 40, "18 Rue Labat 75018 Paris"));
        assertEquals(insert,EleveDAO.addEleve("KAMTO005", "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy"));
        assertEquals(insert,EleveDAO.addEleve("LAURENCY004", "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris"));
        assertEquals(insert,EleveDAO.addEleve("TABIS003", "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris"));
        assertEquals(insert,EleveDAO.addEleve("TAHAE002", "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles"));
        
    }
    
    @AfterAll
    public static void testDeleteListeEleve() {
        int delete = 1;
        int insert = 1;
        assertEquals(delete,EleveDAO.deleteEleveByNum("AGUE001"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("KAMTO005"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("LAURENCY004"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("TABIS003"));
      	assertEquals(delete,EleveDAO.deleteEleveByNum("TAHAE002"));
		System.out.println("Eleve Supprimé ou pas : " + delete);

    }


	@Test
	void testGetEleveByNum() throws SQLException {
		Eleve e_ref= new Eleve("AGUE001", 0, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
		Eleve e_res = new Eleve();
		e_res = EleveDAO.getEleveByNum("AGUE001");	
		System.out.println("Taille ListeEleve by NUM : "+EleveDAO.getAllEleve().size());
		assertEquals(e_ref.getAdresse(),e_res.getAdresse());
		assertEquals(e_ref.getAge(),e_res.getAge());
		assertEquals(e_ref.getNo(),e_res.getNo());
		assertEquals(e_ref.getNom(),e_res.getNom());
		assertEquals(e_ref.getNum(),e_res.getNum());
	}

	@Test
	void testGetEleveByNom() throws SQLException {
		ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

		Eleve e1 = new Eleve("AGUE001", 0, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
//		Eleve e2 = new Eleve("AGUE002", 0, "AGUE MAX", 42, "19 Rue Le Monde Paris");

		listEleve.add(e1);
//		listEleve.add(e2);
		
		System.out.println("Taille Liste NOM: "+EleveDAO.getEleveByNom("AGUE MAX").size());
		for( int i =0; i < listEleve.size(); i++ )
		{
			assertEquals(listEleve.get(i).getAdresse(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getAdresse());
			assertEquals(listEleve.get(i).getNo(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getNo());
			assertEquals(listEleve.get(i).getNom(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getNom());
			assertEquals(listEleve.get(i).getNum(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getNum());
			assertEquals(listEleve.get(i).getAge(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getAge());
		}
	}
	
	
	@Test
	void testGetEleveByNo() throws SQLException {
		ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

		Eleve e5 = new Eleve("TAHAE002", 1, "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles");

		listEleve.add(e5);
		
		System.out.println("Taille Liste NO : "+EleveDAO.getEleveByNo(1).size());
		for( int i =0; i < listEleve.size(); i++ )
		{
			assertEquals(listEleve.get(i).getAdresse(),EleveDAO.getEleveByNo(1).get(i).getAdresse());
			assertEquals(listEleve.get(i).getNo(),EleveDAO.getEleveByNo(1).get(i).getNo());
			assertEquals(listEleve.get(i).getNom(),EleveDAO.getEleveByNo(1).get(i).getNom());
			assertEquals(listEleve.get(i).getNum(),EleveDAO.getEleveByNo(1).get(i).getNum());
			assertEquals(listEleve.get(i).getAge(),EleveDAO.getEleveByNo(1).get(i).getAge());
		}
	}

	@Test
	void testGetListEleveByDateN() throws SQLException {
		ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

		Eleve e1 = new Eleve("TABIS003", 0, "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris");
		Eleve e2 = new Eleve("TAHAE002", 1, "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles");

		listEleve.add(e1);
		listEleve.add(e2);
		
		System.out.println("Taille Liste DateN : "+EleveDAO.getEleveByDateN(1990).size());
		for( int i =0; i < listEleve.size(); i++ )
		{
			assertEquals(listEleve.get(i).getAdresse(),EleveDAO.getEleveByDateN(1990).get(i).getAdresse());
			assertEquals(listEleve.get(i).getNo(),EleveDAO.getEleveByDateN(1990).get(i).getNo());
			assertEquals(listEleve.get(i).getNom(),EleveDAO.getEleveByDateN(1990).get(i).getNom());
			assertEquals(listEleve.get(i).getNum(),EleveDAO.getEleveByDateN(1990).get(i).getNum());
			assertEquals(listEleve.get(i).getAge(),EleveDAO.getEleveByDateN(1990).get(i).getAge());
		}
	}

	@Test
	void testGetAllEleve() throws SQLException {
		ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

		Eleve e1 = new Eleve("AGUE001", 0, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
		Eleve e2 = new Eleve("KAMTO005", 0, "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy");
		Eleve e3 = new Eleve("LAURENCY004", 0, "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris");
		Eleve e4 = new Eleve("TABIS003", 0, "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris");
		Eleve e5 = new Eleve("TAHAE002", 1, "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles");

		listEleve.add(e1);
		listEleve.add(e2);
		listEleve.add(e3);
		listEleve.add(e4);
		listEleve.add(e5);
		
		System.out.println("Taille Liste All : "+EleveDAO.getAllEleve().size() +" et "+listEleve.size());
		for( int i =0; i < listEleve.size(); i++ )
		{
			assertEquals(listEleve.get(i).getAdresse(),EleveDAO.getAllEleve().get(i).getAdresse());
			assertEquals(listEleve.get(i).getNo(),EleveDAO.getAllEleve().get(i).getNo());
			assertEquals(listEleve.get(i).getNom(),EleveDAO.getAllEleve().get(i).getNom());
			System.out.println("Nom 1 : "+EleveDAO.getAllEleve().get(i).getNom() +" et "+listEleve.get(i).getNom());
			assertEquals(listEleve.get(i).getNum(),EleveDAO.getAllEleve().get(i).getNum());
			assertEquals(listEleve.get(i).getAge(),EleveDAO.getAllEleve().get(i).getAge());
		}
	}

	@Test
	void testDeleteEleveByNum() {
		int delete = 1;
		int insert = 1;
        assertEquals(insert,EleveDAO.addEleve("LILY001", "LILIANA AFONSO", 22, "6 rue de Paris 75014 Paris"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("LILY001"));
        assertNotEquals(delete,EleveDAO.deleteEleveByNum("ANDRE001"));
	}

	@Test
	void testUpdAdresseEleveByNum() throws SQLException {
		int update = 1;
		Eleve e = new Eleve("TAHAE002", 0, "TAHA RIDENE", 30, "6 rue de Paris 75014 Paris");
        assertEquals(update,EleveDAO.updAdresseEleveByNum("TAHAE002", "6 rue de Paris 75014 Paris"));
        assertEquals(e.getAdresse(),EleveDAO.getEleveByNum("TAHAE002").getAdresse());
        assertNotEquals(update,EleveDAO.updAdresseEleveByNum("MICHOU001", "18 Rue Labat 75018 Paris"));
        assertEquals(update,EleveDAO.updAdresseEleveByNum("TAHAE002", "12 Rue des Chantiers 78000 Versailles"));
	}

	@Test
	void testUpdNoChambreEleveByNum() throws SQLException {
		int update = 1;
        assertEquals(update,EleveDAO.updNoChambreEleveByNum("TAHAE002", 1));
        assertNotEquals(update,EleveDAO.updNoChambreEleveByNum("MICHOU002", 1));
	}

	@Test
	void testAddEleve() {
		int insert = 1;
        int delete = 1;
        assertEquals(insert,EleveDAO.addEleve("LILY001", "LILIANA AFONSO", 22, "6 rue de Paris 75014 Paris"));
        assertNotEquals(insert,EleveDAO.addEleve("AGUE001", "AGUE MAX", 40, "18 Rue Labat 75018 Paris"));
        assertEquals(delete,EleveDAO.deleteEleveByNum("LILY001"));
	}

}
