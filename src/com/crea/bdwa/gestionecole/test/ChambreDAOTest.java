package com.crea.bdwa.gestionecole.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.crea.bdwa.gestionecole.beans.Chambre;
import com.crea.bdwa.gestionecole.dao.ChambreDAO;
import com.crea.bdwa.gestionecole.dao.EleveDAO;

class ChambreDAOTest {
	
	@BeforeAll
    public static void testAddListeChambre() {
        int insert = 1;
		assertEquals(insert,ChambreDAO.addChambre(1, "0", 350));
        assertEquals(insert,ChambreDAO.addChambre(2, "0", 400));
        assertEquals(insert,ChambreDAO.addChambre(3, "0", 150));
        }
    
    @AfterAll
    public static void testDeleteListeChambre() {
        int delete = 1;
        
        assertEquals(delete, ChambreDAO.deleteChambreByNo(1));
        assertEquals(delete, ChambreDAO.deleteChambreByNo(2));
        assertEquals(delete, ChambreDAO.deleteChambreByNo(3));
		System.out.println("Chambre Supprimé ou pas : " + delete);
    }
    
	@Test
	void testGetChambreByNo() throws SQLException {
		Chambre e_ref= new Chambre(1, "0", 350);
		Chambre e_res = new Chambre();
		e_res = ChambreDAO.getChambreByNo(1);
		assertEquals(e_ref.getNo(),e_res.getNo());
		assertEquals(e_ref.getPrix(),e_res.getPrix());
	}

	@Test
	void testGetChambreByPrix() throws SQLException {
		ArrayList<Chambre> listChambre = new ArrayList<Chambre>();

		Chambre e1 = new Chambre(1, "0", 350);

		listChambre.add(e1);
		
		System.out.println("Taille Liste NUM: "+ChambreDAO.getChambreByPrix(350).size());
		for( int i =0; i < listChambre.size(); i++ )
		{
			assertEquals(listChambre.get(i).getNo(),ChambreDAO.getChambreByPrix(350).get(i).getNo());
			assertEquals(listChambre.get(i).getPrix(),ChambreDAO.getChambreByPrix(350).get(i).getPrix());
		}
	}

	@Test
	void testGetAllChambre() throws SQLException {
		ArrayList<Chambre> listChambre = new ArrayList<Chambre>();
		
		Chambre c1 = new Chambre(1, "0", 350);
		Chambre c2 = new Chambre(2, "0", 400);
		Chambre c3 = new Chambre(3, "0", 150);

		listChambre.add(c1);
		listChambre.add(c2);
		listChambre.add(c3);
		
		System.out.println("Taille Liste All : "+ChambreDAO.getAllChambre().size() +" et "+listChambre.size());
		for( int i =0; i < listChambre.size(); i++ )
		{
			assertEquals(listChambre.get(i).getNo(),ChambreDAO.getAllChambre().get(i).getNo());
			assertEquals(listChambre.get(i).getPrix(),ChambreDAO.getAllChambre().get(i).getPrix());
		}
	}

	@Test
	void testDeleteChambreByNo() {
		int delete = 1;
		int insert = 1;
        assertEquals(insert,ChambreDAO.addChambre(4, "0", 650));
        assertEquals(delete,ChambreDAO.deleteChambreByNo(4));
        assertNotEquals(delete,ChambreDAO.deleteChambreByNo(5));
	}

	@Test
	void testUpdNumChambreByNo() throws SQLException {
		int update = 1;
		Chambre c = new Chambre(1, "0", 350);
        assertEquals(update,ChambreDAO.updNumChambreByNo(1, "0"));
        assertEquals(c.getNum(),ChambreDAO.getChambreByNo(1).getNum());
        assertNotEquals(update,ChambreDAO.updNumChambreByNo(4, "0"));
        assertEquals(update,ChambreDAO.updNumChambreByNo(1, "0"));
	}

	@Test
	void testUpdPrixChambreByNo() throws SQLException {
		int update = 1;
		Chambre c = new Chambre(1, "0", 450);
        assertEquals(update,ChambreDAO.updPrixChambreByNo(1, 450));
        assertEquals(c.getPrix(),ChambreDAO.getChambreByNo(1).getPrix());
        assertNotEquals(update,ChambreDAO.updPrixChambreByNo(4, 200));
        assertEquals(update,ChambreDAO.updPrixChambreByNo(1, 350));
	}

	@Test
	void testAddChambre() {
		int insert = 1;
        int delete = 1;
        assertEquals(insert,ChambreDAO.addChambre(4, "0", 420));
        assertNotEquals(insert,ChambreDAO.addChambre(1, "0", 350));
        assertEquals(delete,ChambreDAO.deleteChambreByNo(4));
	}

}
