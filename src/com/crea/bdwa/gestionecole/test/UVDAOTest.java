package com.crea.bdwa.gestionecole.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.crea.bdwa.gestionecole.beans.UV;
import com.crea.bdwa.gestionecole.dao.UVDAO;

class UVDAOTest {
	@BeforeAll 
    public static void testAddListeEleve() {
        int insert = 1;
        assertEquals(insert,UVDAO.addUV("JAVA_Grp1", 30, "Mr RIDENE"));
        assertEquals(insert,UVDAO.addUV("maths_info_Grp1", 26, "Mme ASSELAH"));
        assertEquals(insert,UVDAO.addUV("Web_Service_Grp1", 10, "Mr PLASSE"));
    }
    
    @AfterAll 
    public static void testDeleteListeUV() {
        int delete = 1;
        assertEquals(delete,UVDAO.deleteUVByCode("JAVA_Grp1"));
        assertEquals(delete,UVDAO.deleteUVByCode("maths_info_Grp1"));
        assertEquals(delete,UVDAO.deleteUVByCode("Web_Service_Grp1"));
		System.out.println("Uv Supprimé ou pas : " + delete);
    }
	@Test
	void testGetUVByCode() throws SQLException {
		UV e_ref= new UV("JAVA_Grp1", 30, "Mr RIDENE");
		UV e_res = new UV();
		e_res = UVDAO.getUVByCode("JAVA_Grp1");	
		System.out.println("Taille ListeUV by Code : "+UVDAO.getAllUV().size());
		assertEquals(e_ref.getCode(),e_res.getCode());
		assertEquals(e_ref.getNbh(),e_res.getNbh());
		assertEquals(e_ref.getCoord(),e_res.getCoord());
	}

	@Test
	void testGetUVByNbh() throws SQLException {
		ArrayList<UV> listUV = new ArrayList<UV>();

		UV u1 = new UV("JAVA_Grp1", 30, "Mr RIDENE");

		listUV.add(u1);
//		listEleve.add(e2);
		
		System.out.println("Taille Liste Nbh: "+UVDAO.getUVByNbh(30).size());
		for( int i =0; i < listUV.size(); i++ )
		{
			assertEquals(listUV.get(i).getCode(),UVDAO.getUVByNbh(30).get(i).getCode());
			assertEquals(listUV.get(i).getNbh(),UVDAO.getUVByNbh(30).get(i).getNbh());
		}
	}


	@Test
	void testGetAllUV() throws SQLException {
		ArrayList<UV> listUV = new ArrayList<UV>();

		UV u1 = new UV("JAVA_Grp1", 30, "Mr RIDENE");
		UV u2 = new UV("maths_info_Grp1", 26, "Mme ASSELAH");
		UV u3 = new UV("Web_Service_Grp1", 10, "Mr PLASSE");

		listUV.add(u1);
		listUV.add(u2);
		listUV.add(u3);
		
		System.out.println("Taille Liste Coord: "+UVDAO.getAllUV().size());
		for( int i =0; i < listUV.size(); i++ )
		{
			assertEquals(listUV.get(i).getCode(),UVDAO.getAllUV().get(i).getCode());
			assertEquals(listUV.get(i).getNbh(),UVDAO.getAllUV().get(i).getNbh());
		}
	}

	@Test
	void testDeleteUVByCode() {
		int delete = 1;
		int insert = 1;
        assertEquals(insert,UVDAO.addUV("TESTDELETE", 37, "Lune"));
        assertEquals(delete,UVDAO.deleteUVByCode("TESTDELETE"));
        assertNotEquals(delete,UVDAO.deleteUVByCode("Unknown"));
	}

	@Test
	void testUpdNbhUVByCode() throws SQLException {
		int update = 1;
		UV u = new UV("JAVA_Grp1", 27, "Mr RIDENE");
		
        assertEquals(update,UVDAO.updNbhUVByCode("JAVA_Grp1", 27));
        assertEquals(u.getNbh(),UVDAO.getUVByCode("JAVA_Grp1").getNbh());
        assertNotEquals(update,UVDAO.updNbhUVByCode("MICHOU001", 12));
        assertEquals(update,UVDAO.updNbhUVByCode("JAVA_Grp1", 30));
	}

	

	@Test
	void testAddUV() {
		int insert = 1;
        int delete = 1;
        assertEquals(insert,UVDAO.addUV("TESTDELETE", 37, "Lune"));
        assertNotEquals(insert,UVDAO.addUV("JAVA_Grp1", 30, "Mr RIDENE"));
        assertEquals(delete,UVDAO.deleteUVByCode("TESTDELETE"));
	}

}
