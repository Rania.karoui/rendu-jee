package com.crea.bdwa.gestionecole.dao;

import java.sql.SQLException;
import java.util.ArrayList;


import com.crea.bdwa.gestionecole.beans.Inscrit;
import com.crea.bdwa.gestionecole.utils.DBAction;


public class InscritDAO {
	public static ArrayList<Inscrit> fillArray(String req, ArrayList<Inscrit> listInscrit) throws SQLException {
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{
			Inscrit inscritTemp = new Inscrit();
			inscritTemp.setCode(DBAction.getRes().getString(1));
			inscritTemp.setNum(DBAction.getRes().getString(2));
			inscritTemp.setNote(DBAction.getRes().getInt(3));
			listInscrit.add(inscritTemp);
		}
		DBAction.DBClose();
		return listInscrit;
	}
	
	public static Inscrit getInscritByCodeNum(String code, String num) throws SQLException 
	{
		Inscrit inscritTemp = new Inscrit();
		String req = "SELECT * FROM inscrit WHERE code = '"+code+"' AND num ='"+num+"' ";
		
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{	
			inscritTemp.setCode(DBAction.getRes().getString(1));
			inscritTemp.setNum(DBAction.getRes().getString(2));
			inscritTemp.setNote(DBAction.getRes().getInt(3));
		}
		DBAction.DBClose();
		return inscritTemp;
	}
	
	public static ArrayList<Inscrit> getInscritByCode(String code) throws SQLException 
	{
		ArrayList<Inscrit> listNoteCode = new ArrayList<Inscrit>();		
		String req = "SELECT * FROM inscrit WHERE code = '"+code+"' ";
		
		fillArray(req, listNoteCode);
		
		return listNoteCode;
	}
	
	public static ArrayList<Inscrit> getInscritByNum(String num) throws SQLException 
	{
		ArrayList<Inscrit> listNoteNum = new ArrayList<Inscrit>();		
		String req = "SELECT * FROM inscrit WHERE num = '"+num+"' ";
		
		fillArray(req, listNoteNum);
		
		return listNoteNum;
	}

	public static ArrayList<Inscrit> getInscritByNote(int note) throws SQLException {
		ArrayList<Inscrit> listInscrit = new ArrayList<Inscrit>();
		String req = "SELECT * FROM inscrit WHERE note = " + note + "";
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			Inscrit inscritTemp = new Inscrit();
			inscritTemp.setCode(DBAction.getRes().getString(1));
			inscritTemp.setNum(DBAction.getRes().getString(2));
			inscritTemp.setNote(DBAction.getRes().getInt(3));
			listInscrit.add(inscritTemp);
		}
		for (int i = 0; i < listInscrit.size(); i++) {
			listInscrit.get(i).affiche();
		}
		DBAction.DBClose();
		return listInscrit;
	}

	//Get all
	public static ArrayList<Inscrit> getAllInscrit() throws SQLException 
	{
		ArrayList<Inscrit> listAllInscrit = new ArrayList<Inscrit>();		
		String req = "SELECT * FROM inscrit";

		fillArray(req, listAllInscrit);
		
		return listAllInscrit;
	}
	
	//Delete
	public static int deleteInscritByCodeNum(String code, String num) {
		int result = -1;
		DBAction.DBConnexion();
		String req = "DELETE FROM inscrit WHERE code = '"+code+"' AND num ='"+num+"' ";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executée");	
		} 
		catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());	
		}
		System.out.println("["+req+"] Suppression : Valeur de result == "+result);
		DBAction.DBClose();
		return result;
	}
	
	//Update
	public static int updateNumInscritByCodeNum(String code, String num, float note)  
			throws SQLException
	{
		int result = -1;
		DBAction.DBConnexion();
		String req = "UPDATE inscrit SET note = '"+note+"' WHERE code = '"+code+"' AND num ='"+num+"' ";
		result = DBAction.getStm().executeUpdate(req);
		System.out.println("Requete executee");	
		
		DBAction.DBClose();
		return result;
	}
	
	//Add
	public static int addInscrit(String code, String num, float note) 
	{
		int result = -1;
		DBAction.DBConnexion();

		String req = "INSERT INTO inscrit (code, num, note)"
				+ " VALUES ('"+code+"','"+num+"',"+note+") ";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		
		DBAction.DBClose();
		return result;
	}
}
