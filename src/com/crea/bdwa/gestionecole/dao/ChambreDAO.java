package com.crea.bdwa.gestionecole.dao;

import java.sql.SQLException;
import java.util.ArrayList;


import com.crea.bdwa.gestionecole.beans.Chambre;
import com.crea.bdwa.gestionecole.utils.DBAction;


public class ChambreDAO {
	public static ArrayList<Chambre> fillArray(String req, ArrayList<Chambre> listChambre) throws SQLException {
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{
			Chambre chambreTemp = new Chambre();
			chambreTemp.setNo(DBAction.getRes().getInt(1));
			chambreTemp.setNum(DBAction.getRes().getString(2));
			chambreTemp.setPrix(DBAction.getRes().getInt(3));
			listChambre.add(chambreTemp);
		}
		for(int i =0; i<listChambre.size();i++) {
			listChambre.get(i).affiche();
		}
		DBAction.DBClose();
		return listChambre;
	}
	
	public static Chambre getChambreByNo(int no) throws SQLException 
	{
		Chambre chambreTemp = new Chambre();
		String req = "SELECT no, num, prix FROM chambre WHERE no = '"+no+"' ";
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{	
			chambreTemp.setNo(DBAction.getRes().getInt(1));
			chambreTemp.setNum(DBAction.getRes().getString(2));
			chambreTemp.setPrix(DBAction.getRes().getInt(3));
			chambreTemp.affiche();
		}
		DBAction.DBClose();
		return chambreTemp;
	}
	
	public static ArrayList<Chambre> getChambreByNum(String num) throws SQLException 
	{
		ArrayList<Chambre> listChambreNo = new ArrayList<Chambre>();		
		String req = "SELECT no, num, prix FROM chambre WHERE num = '"+num+"' ";
		
		fillArray(req, listChambreNo);
		
		return listChambreNo;
	}
	
	public static ArrayList<Chambre> getChambreByPrix(float prix) throws SQLException 
	{
		ArrayList<Chambre> listChambrePrix = new ArrayList<Chambre>();		
		String req = "SELECT no, num, prix FROM chambre WHERE prix = '"+prix+"' ";

		fillArray(req, listChambrePrix);
		
		return listChambrePrix;
	}
	
	public static ArrayList<Chambre> getAllChambre() throws SQLException 
	{
		ArrayList<Chambre> listAllChambre = new ArrayList<Chambre>();		
		String req = "SELECT * FROM chambre";

		fillArray(req, listAllChambre);
		
		return listAllChambre;
	}
	
	
	public static int deleteChambreByNo(int no) {
		int result = -1;
		DBAction.DBConnexion();
		String req = "DELETE FROM chambre WHERE no = '"+no+"';";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executée");	
		} 
		catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());	
		}
		System.out.println("["+req+"] Suppression : Valeur de result == "+result);
		DBAction.DBClose();
		return result;
	}
	
	
	public static int updNumChambreByNo(int no,String num)  
			throws SQLException
	{
		int result = -1;
		DBAction.DBConnexion();
		String req = "UPDATE chambre SET num = '"+num+"' WHERE no ='"+no+"' ";
		result = DBAction.getStm().executeUpdate(req);
		System.out.println("Requete executee");	
		
		DBAction.DBClose();
		return result;
	}
	
	public static int updPrixChambreByNo(int no,float prix)  
			throws SQLException
	{
		int result = -1;
		DBAction.DBConnexion();
		String req = "UPDATE chambre SET prix = '"+prix+"' WHERE no ='"+no+"' ";
		result = DBAction.getStm().executeUpdate(req);
		System.out.println("Requete executee");	
		
		DBAction.DBClose();
		return result;
	}
	
	public static int addChambre(int no,String num, float prix) 
	{
		int result = -1;
		DBAction.DBConnexion();

		String req = "INSERT INTO chambre (no, num, prix)"
				+ " VALUES ('"+no+"','"+num+"',"+prix+");";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		
		DBAction.DBClose();
		return result;
	}
	

}
