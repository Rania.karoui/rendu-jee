
package com.crea.bdwa.gestionecole.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;


import com.crea.bdwa.gestionecole.beans.Eleve;
import com.crea.bdwa.gestionecole.utils.DBAction;


public class EleveDAO 
{
	/**
	 * Recup�rer un eleve par son numero
	 * 
	 * @param num : numero de l'eleve
	 * @return : un eleve retrouve sinon null
	 */
	public static Eleve getEleveByNum(String num) throws SQLException 
	{
		Eleve elevTemp = new Eleve();
		String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE num = '"+num+"' ";//SELECT * FROM Eleve where num = \""+num+"\""
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{	
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setNo(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setAge(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
		}
		DBAction.DBClose();
		return elevTemp;
	}
	/**
	 * Récupérer une liste d'élèves par leurs noms ;
	 * 
	 * @param nom : nom de l'élève
	 * @return : une liste d'élèves retrouvés sinon null
	 */
	public static ArrayList<Eleve> getEleveByNom(String listsnoms) throws SQLException 
//	public static ArrayList<Eleve> getEleveByNom(ArrayList<String> lstnoms) throws SQLException 
	{
		//Cr�ation de ma liste d'�l�ve ayant le meme nom
		ArrayList<Eleve> listEleveNom = new ArrayList<Eleve>();		
		String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE nom = '"+listsnoms+"' ";//SELECT * FROM Eleve where num = \""+num+"\""

/*		String req2 = "SELECT * FROM Eleve where nom IN (" ;
		for(int i= 0; i< lstnoms.size();i++) {
			req2+= lstnoms.get(i).toString()+ ",";
		}
		req2+=")";
	*/	

		// Connexion
		DBAction.DBConnexion();//System.out.println(req);
		// ex�cution de la requ�te et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		// Creation de l'objet eleveTemp � travers le ResultSet BD
		while( DBAction.getRes().next() )
		{
	/**		Eleve elevTemp = new Eleve(DBAction.getRes().getString(1),
					DBAction.getRes().getInt(2), DBAction.getRes().getString(3), 
					DBAction.getRes().getInt(4), DBAction.getRes().getString(5));*/
			Eleve elevTemp = new Eleve();
			//Creation de l'objet eleveTemp � travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setNo(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setAge(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleveNom.add(elevTemp);
		}
		for(int i =0; i<listEleveNom.size();i++) {
			listEleveNom.get(i).affiche();
		}
		// Fermeture de la connexion
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return listEleveNom;
	}
	/**
	 * Recuperer les eleves par leurs numero de chambre ;
	 * 
	 * @param no_ch : numero de chambre
	 * @return : une liste d'eleves retrouves sinon null
	 */
	public static ArrayList<Eleve> getEleveByNo(int no) throws SQLException 
	{ 
		//Cr�ation de ma liste d'�l�ve partageant la m�me chambre
		ArrayList<Eleve> listEleveNo = new ArrayList<Eleve>();
		
		String req = "SELECT * FROM eleve where no = "+no+";";
		// Connexion
		DBAction.DBConnexion();//System.out.println(req);
		
		// ex�cution de la requ�te et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		
		// Creation de l'objet eleveTemp � travers le ResultSet BD
		while( DBAction.getRes().next() )
		{	
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp � travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setNo(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setAge(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleveNo.add(elevTemp);
		}
		
		// Fermeture de la connexion
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return listEleveNo;
	}
	/**
	 * R�cup�rer un ensemble d��l�ve qui ont la m�me date de naissance
	 * 
	 * @param d : date de naissance
	 * @return liste des �l�ve oubien null si aucun �l�ve ne correspond � la date de
	 *         naissance
	 */
	public static ArrayList<Eleve> getEleveByDateN(int anneeNaissance) throws SQLException
	{
		ArrayList<Eleve> listEleveAnneeNaissance = new ArrayList<Eleve>();
		//on recupere l'ann�e en cours
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		 
		 //Variable � passer en param�tre dans la requ�te pour avoir les �l�ves d'une tranche d'age:
		//Je fais la diff�rence entre l'ann�e en cours et l'ann�e pass�e en param�tre de la fonction.
		int ageEleves = year - anneeNaissance;
		String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE age ="+ageEleves+" ";
		// Connexion
		DBAction.DBConnexion();
		// ex�cution de la requ�te et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		
		while( DBAction.getRes().next() )
		{	
			//Instanciation de mon objet Eleve
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp � travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setNo(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setAge(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleveAnneeNaissance.add(elevTemp);
		}
		// Fermeture de la connexion
		DBAction.DBClose();
		return listEleveAnneeNaissance;
		
	}
	/**
	 * R�cup�rer la liste de tout les �l�ves
	 * 
	 * @return la liste de tout les �l�ves
	 */
	public static ArrayList<Eleve> getAllEleve() throws SQLException 
	{
		
		ArrayList<Eleve> listEleve = new ArrayList<Eleve>();
		
		String req = "SELECT num, no, nom, age, adresse FROM eleve ";
		// Connexion
		DBAction.DBConnexion();
		// ex�cution de la requ�te et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		
		while( DBAction.getRes().next() )
		{	
			//Instanciation de mon objet Eleve
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp � travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setNo(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setAge(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleve.add(elevTemp);
		}
		// Fermeture de la connexion
		DBAction.DBClose();
		return listEleve;
	}
	/**
	 * Supprimer un �l�ve par son num�ro
	 * 
	 * @param num : num�ro de l'�l�ve � supprimer
	 * @return : 1 si l'�l�ve a �t� retrouv� et surpprim� oubien 0 s'il n'a pas �t�
	 *         retrouv�, sinon code d'�rreur s'il y a eu une erreur � l'�chelle de
	 *         la BD
	 */
	public static int deleteEleveByNum(String num) {
		int result = -1;
		DBAction.DBConnexion();
		String req = "DELETE FROM eleve WHERE num = '"+num+"' ";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executée");	
		} 
		catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());	
		}
		System.out.println("[" + req + "] Suppression : Valeur result = " + result);
		DBAction.DBClose();
		return result;

		/**
	 * R�cup�rer un ensemble d��l�ve qui ont la m�me date de naissance
	 * 
	 * @param d : date de naissance
	 * @return liste des �l�ve oubien null si aucun �l�ve ne correspond � la date de
	 *         naissance
	 */
	public static ArrayList<Eleve> getLstElevesByDateNaissance(int anneeNaissance) throws SQLException {
		ArrayList<Eleve> listEleveAnneeNaissance = new ArrayList<Eleve>();
		// on recupere l'ann�e en cours
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);

		// Variable � passer en param�tre dans la requ�te pour avoir les �l�ves d'une
		// tranche d'age:
		// Je fais la diff�rence entre l'ann�e en cours et l'ann�e pass�e en param�tre
		// de la fonction.
		int anneeNaissanceEleves = year - anneeNaissance;
		String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE age =" + anneeNaissanceEleves + " ";
		// Connexion
		DBAction.DBConnexion();
		// ex�cution de la requ�te et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));

		while (DBAction.getRes().next()) {
			// int i = 0;
			// Instanciation de mon objet Eleve
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp � travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setNo(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setAge(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleveAnneeNaissance.add(elevTemp);
			// i = i + 1;
		}
		// Fermeture de la connexion
		DBAction.DBClose();
		return listEleveAnneeNaissance;
	}
	}
	/**
	 * Mettre � jour l�adresse d�un �l�ve identifi� par son num�ro
	 * 
	 * @param num        : le num�ro de l'�l�ve
	 * @param newAdresse : la nouvelle adresse � mettre � jour
	 * @return 1 si mise � jour oubien 0 si rien n'a �t� mis � jour sinon
	 *         -code_erreur s'il y a eu une erreur � l'�chelle de la BD
	 */
	public static int updAdresseEleveByNum(String num,String adresse)  
			throws SQLException
	{
		int result = -1;
		DBAction.DBConnexion();
		String req = "UPDATE eleve SET adresse = '"+adresse+"' WHERE num ='"+num+"' ";
		result = DBAction.getStm().executeUpdate(req);
		System.out.println("Requete executee");	
		
		DBAction.DBClose();
		return result;
	}
	
	// �
	/**
	 * Mettre � jour le num�ro d�un �l�ve identifi� par son num�ro
	 * 
	 * @param num   : le num�ro de l'�l�ve
	 * @param newNo : la nouvelle chambre
	 * @return 1 si mise � jour oubien 0 oubien code d'�rreur s'il y a eu une erreur
	 *         � l'�chelle de la BD
	 */
	public static int updateEleveNumChambreByNum(String num, int newNo) {
		int result = -1;
		DBAction.DBConnexion();

		String req = "UPDATE eleve SET no = NULL WHERE num = '" + num + "'";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
			System.out.println(ex + " adedigato");
		}
		DBAction.DBClose();
		return result;
	}

	/**
	 * Ajouter un nouvel eleve
	 * 
	 * @param new_eleve : le nouvel eleve a jouter
	 * @return 1 si ajout 0 sinon oubien le code d'erreur s'il y a eu une erreur 
	 *         l'echelle de la B
	 */
	public static int addEleve(String num,String nom, int age, String adresse) 
	{
		int result = -1;
		DBAction.DBConnexion();

		String req = "INSERT INTO eleve (num, no, nom, age, adresse)"
				+ " VALUES ('"+num+"',NULL,'"+nom+"',"+age+",'"+adresse+"') ";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		// System.out.println("["+req+"] Valeur de result == "+result);
		// System.out.println(req);	
		DBAction.DBClose();
		return result;
	}

	
}