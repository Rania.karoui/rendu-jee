package com.crea.bdwa.gestionecole.dao;

import java.sql.SQLException;
import java.util.ArrayList;


import com.crea.bdwa.gestionecole.beans.UV;
import com.crea.bdwa.gestionecole.utils.DBAction;


public class UVDAO {
	public static ArrayList<UV> fillArray(String req, ArrayList<UV> listUV) throws SQLException {
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{
			UV uvTemp = new UV();
			uvTemp.setCode(DBAction.getRes().getString(1));
			uvTemp.setNbh(DBAction.getRes().getInt(2));
			uvTemp.setCoord(DBAction.getRes().getString(3));
			listUV.add(uvTemp);
		}
		DBAction.DBClose();
		return listUV;
	}
	
	public static UV getUVByCode(String code){
		UV uvTemp = new UV();
		String req = "SELECT code, nbh, coord FROM uv WHERE code = '"+code+"' ";
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{	
			uvTemp.setCode(DBAction.getRes().getString(1));
			uvTemp.setNbh(DBAction.getRes().getInt(2));
			uvTemp.setCoord(DBAction.getRes().getString(3));
		}
		DBAction.DBClose();
		return uvTemp;
	}
	
	public static ArrayList<UV> getUVByNbh(int nbh) throws SQLException 
	{
		ArrayList<UV> listUVCode = new ArrayList<UV>();		
		String req = "SELECT code, nbh, coord FROM uv WHERE nbh = '"+nbh+"' ";
		
		fillArray(req, listUVCode);
		
		return listUVCode;
	}
	
	public static ArrayList<UV> getAllUV() throws SQLException 
	{
		ArrayList<UV> listAllUV = new ArrayList<UV>();		
		String req = "SELECT * FROM uv ";
		
		fillArray(req, listAllUV);
		
		return listAllUV;
	}
	
	public static int deleteUVByCode(String code) {
		int result = -1;
		DBAction.DBConnexion();
		String req = "DELETE FROM uv WHERE code = '"+code+"' ";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executée");	
		} 
		catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());	
		}
		DBAction.DBClose();
		return result;
	}
	
	public static int updNbhUVByCode(String code,int nbh)  
			throws SQLException
	{
		int result = -1;
		DBAction.DBConnexion();
		String req = "UPDATE uv SET nbh = '"+nbh+"' WHERE code ='"+code+"' ";
		result = DBAction.getStm().executeUpdate(req);
		System.out.println("Requete executee");	
		
		DBAction.DBClose();
		return result;
	}
	
	
	public static int addUV(String code, int nbh, String coord) {int result = -1;
		DBAction.DBConnexion();
	
		String req = "INSERT INTO uv (code, nbh, coord)"
				+ " VALUES ('"+code+"',"+nbh+",'"+coord+"')";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		
		DBAction.DBClose();
		return result;
	}
}
