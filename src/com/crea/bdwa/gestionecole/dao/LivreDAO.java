package com.crea.bdwa.gestionecole.dao;

import java.sql.SQLException;
import java.util.ArrayList;


import com.crea.bdwa.gestionecole.beans.Livre;
import com.crea.bdwa.gestionecole.utils.DBAction;


public class LivreDAO {
	public static ArrayList<Livre> fillArray(String req, ArrayList<Livre> listLivre) throws SQLException {
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{
			Livre livreTemp = new Livre();
			livreTemp.setCote(DBAction.getRes().getString(1));
			livreTemp.setNum(DBAction.getRes().getString(2));
			livreTemp.setTitre(DBAction.getRes().getString(3));
			livreTemp.setDatepret(DBAction.getRes().getDate(4));
			listLivre.add(livreTemp);
		}
		DBAction.DBClose();
		return listLivre;
	}
	
	public static Livre getLivreByCote(String cote) throws SQLException 
	{
		Livre chambreTemp = new Livre();
		String req = "SELECT cote, num, titre, datepret FROM livre WHERE cote = '"+cote+"' ";
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while( DBAction.getRes().next() )
		{	
			chambreTemp.setCote(DBAction.getRes().getString(1));
			chambreTemp.setNum(DBAction.getRes().getString(2));
			chambreTemp.setTitre(DBAction.getRes().getString(3));
			chambreTemp.setDatepret(DBAction.getRes().getDate(4));
		}
		DBAction.DBClose();
		return chambreTemp;
	}
	
	
	public static ArrayList<Livre> getLivreByTitre(String titre) throws SQLException 
	{
		ArrayList<Livre> listLivreTitre = new ArrayList<Livre>();		
		String req = "SELECT * FROM livre WHERE titre = '" + titre + "' ";
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			Livre livreTemp = new Livre();
			livreTemp.setCote(DBAction.getRes().getString(1));
			livreTemp.setNum(DBAction.getRes().getString(2));
			livreTemp.setTitre(DBAction.getRes().getString(3));
			livreTemp.setDatepret(DBAction.getRes().getString(4));
			listLivre.add(livreTemp);
		}
		for (int i = 0; i < listLivre.size(); i++) {
			listLivre.get(i).affiche();
		}
		DBAction.DBClose();
		return listLivre;
	}
	
	
	public static ArrayList<Livre> getAllLivre() throws SQLException
	{
		ArrayList<Livre> listAllLivre = new ArrayList<Livre>();		
		String req = "SELECT * FROM livre";
		DBAction.DBConnexion();
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			Livre livreTemp = new Livre();
			livreTemp.setCote(DBAction.getRes().getString(1));
			livreTemp.setNum(DBAction.getRes().getString(2));
			livreTemp.setTitre(DBAction.getRes().getString(3));
			livreTemp.setDatepret(DBAction.getRes().getString(4));
			listLivre.add(livreTemp);
		}
		DBAction.DBClose();
		return listLivre;
	}
	
	public static int deleteLivreByCote(String cote) {
		int result = -1;
		DBAction.DBConnexion();
		String req = "DELETE FROM livre WHERE cote = '"+cote+"' ";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executée");	
		} 
		catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());	
		}
		DBAction.DBClose();
		return result;
	}
	
	public static int updateTitreLivreByCote(String cote,String num)  
		throws SQLException
	{
		int result = -1;
		DBAction.DBConnexion();

		String req = "UPDATE livre SET titre = '" + newTitre + "' WHERE cote = '" + cote + "'";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executee");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
		}
		DBAction.DBClose();
		return result;
	}
	
	public static int addLivre(String cote,String num, String titre, Date datepret) 
	{
		int result = -1;
		DBAction.DBConnexion();
		String req = "INSERT INTO livre (cote, num, titre, datepret)"
				+ " VALUES ('"+cote+"','"+num+"','"+titre+"','"+df.format(datepret)+"') ";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex)
		{
			result = - ex.getErrorCode();
			System.out.println(ex.getMessage());
		}	
		DBAction.DBClose();
		return result;
	}
}
