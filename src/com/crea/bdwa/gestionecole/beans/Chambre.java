package com.crea.bdwa.gestionecole.beans;


public class Chambre {
	//Attributs
	private int no;
	private String num;
	private float prix;

// Constructeur
	public Chambre(int no, String num, float prix) {
		super();
		this.no = no;
		this.num = num;
		this.prix = prix;
	}

	public Chambre() {
		this(0, "", 0);
	}

// Getters & Setters

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}
	
	public void affiche() {
		System.out.println("Num chambre : [" + this.no + "] Num élève: [" + this.num
				+ "]  Prix chambre : [" + this.prix + "]  ");
	}
}
