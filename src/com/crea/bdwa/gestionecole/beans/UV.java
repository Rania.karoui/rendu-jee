package com.crea.bdwa.gestionecole.beans;

public class UV {
	// Attributs
	private String code;
	private int nbh;
	private String coord;

	// Constructeur
	public UV(String code, int nbh, String coord) {
		super();
		this.code = code;
		this.nbh = nbh;
		this.coord = coord;
	}

	public UV() {
		this("", 0, "");
	}

	// Getters & Setters
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getNbh() {
		return nbh;
	}

	public void setNbh(int nbh) {
		this.nbh = nbh;
	}

	public String getCoord() {
		return coord;
	}

	public void setCoord(String coord) {
		this.coord = coord;
	}

	// Methods
	public void affiche() {
		System.out.println(" code " + code + " nbh " + nbh + " coord " + coord);
	}
}
