package com.crea.bdwa.gestionecole.beans;

import java.util.Date;


public class Livre {
	// Attributs
	private String cote;
	private String num;
	private String titre;
	private Date datepret;

	// Constructeur
	public Livre(String cote, String num, String titre, Date datepret) {
		this.cote = cote;
		this.num = num;
		this.titre = titre;
		this.datepret = datepret;
	}

	public Livre() {
		this("", "", "", null);
	}

	// Getters & Setters
	public String getCote() {
		return cote;
	}

	public void setCote(String cote) {
		this.cote = cote;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Date getDatepret() {
		return datepret;
	}

	public void setDatepret(Date datepret) {
		this.datepret = datepret;
	}
	// Methods
	public void affiche() {
	System.out.println("cote " + cote + " num " + num + "titre " + titre + "datepret" + datepret);
}
}
